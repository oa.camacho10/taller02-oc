var assert = require('assert');
describe('los estudiantes login', function() {
   it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
		browser.waitForVisible('button=Ingresar', 2000);
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });
	
	// 1.1 Registro	Correcto	
	it('register user', function () {
		var cajaSignUp = browser.element('.cajaSignUp');
        
		var nombreInput = cajaSignUp.element('input[name="nombre"]');
		nombreInput.click();
        nombreInput.keys('Omar');	
		
		var apellidoInput = cajaSignUp.element('input[name="apellido"]');
		apellidoInput.click();
        apellidoInput.keys('Camacho');
		
		var correoInput = cajaSignUp.element('input[name="correo"]');
		correoInput.click();
        correoInput.keys('oacaza.camacho10@uniandes.edu.co');
		
		var idDepartamentoSelect = cajaSignUp.element('select[name="idDepartamento"]');
		idDepartamentoSelect.click();
        idDepartamentoSelect.selectByValue(14);
		
		var passwordInput = cajaSignUp.element('input[name="password"]');
		passwordInput.click();
        passwordInput.keys('123456');
		
		var aceptaInput = cajaSignUp.element('input[name="acepta"]');
		aceptaInput.click();
		
		cajaSignUp.element('button=Registrarse').click()
		browser.waitForVisible('.sweet-alert', 5000);

		var alertText = browser.element('.sweet-alert').getText();
        expect(alertText).toContain("Registro exitoso!");
	
	});
	
	// 1.2 creación de una cuenta con un login que ya existe
	it('Creating an account with a login that already exists', function () {
		browser.click('button=OK');
		var cajaSignUp = browser.element('.cajaSignUp');
        
		var nombreInput = cajaSignUp.element('input[name="nombre"]');
		nombreInput.clearElement();
		nombreInput.click();
        nombreInput.keys('Omar');	
		
		var apellidoInput = cajaSignUp.element('input[name="apellido"]');
		apellidoInput.clearElement();
		apellidoInput.click();
        apellidoInput.keys('Camacho');
		
		var correoInput = cajaSignUp.element('input[name="correo"]');
		correoInput.clearElement();
		correoInput.click();
        correoInput.keys('oacaza.camacho10@uniandes.edu.co');
		
		var idDepartamentoSelect = cajaSignUp.element('select[name="idDepartamento"]');
		idDepartamentoSelect.click();
        idDepartamentoSelect.selectByValue(14);
		
		var passwordInput = cajaSignUp.element('input[name="password"]');
		passwordInput.clearElement();
		passwordInput.click();
        passwordInput.keys('123456');
		
		var aceptaInput = cajaSignUp.element('input[name="acepta"]');
		if(!aceptaInput.isSelected()){
			aceptaInput.click();
		}
		
		var cajaFormSignUp = cajaSignUp.element('.loginForm');
		cajaFormSignUp.element('.logInButton').click()
		browser.waitForVisible('.sweet-alert', 5000);

		var alertText = browser.element('.sweet-alert').getText();
        expect(alertText).toContain("Ya existe un usuario registrado con el correo");
	
	});
	
	// 1.3 Login Correcto
	it('Successful login', function () {
	
        browser.click('button=OK');
		
		var cajaLogIn = browser.element('.cajaLogIn');
        
		var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.clearElement();
		mailInput.click();
		browser.waitForVisible('button=Ingresar', 2000);
		
        mailInput.keys('ocz.camacho10@uniandes.edu.co');

        var passwordInput = cajaLogIn.element('input[name="password"]');
		passwordInput.clearElement();
        passwordInput.click();
        passwordInput.keys('123456');

        cajaLogIn.element('.logInButton').click();
		browser.waitForVisible('.botonDropdown', 5000);
		
        var alertText = browser.element('input[id="cuenta"]').selector;
        expect(alertText).toBe('input[id="cuenta"]');
	
	});
	
	// 2. Pruebe la funcionalidad de búsqueda de profesores
	it("Teacher search, Go to a teacher's page", function () {
		
		browser.waitForVisible('button=Ingresar', 5000);
		
		var searchInput = browser.element('.Select-control');
        searchInput.click();
        searchInput.keys('jose ti');
		//browser.waitForVisible('.Select-control', 5000);
		//browser.click('.Select-control');
		//browser.waitForVisible('button=Ingresar', 5000);
		//browser.waitForVisible('.Select-control', 5000);
		//var alertText = browser.element('.descripcionProfesor').getText();
		//console.log('.--.-.-------------');
		//console.log(alertText);
		//console.log('.--.-.-------------');
		
		//browser.element('.Select-menu-outer').click();		
		
	});
});