module.exports = { // adapted from: https://git.io/vodU0
  'Los estudiantes login falied': function(browser) {
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.botonIngresar')
	  .click('.botonIngresar')
	  .click('.botonIngresar')
      .setValue('.cajaLogIn input[name="correo"]', 'wrongemail1@example.com')
      .setValue('.cajaLogIn input[name="password"]', '1234')
      .click('.cajaLogIn .logInButton')
      .waitForElementVisible('.aviso.alert.alert-danger', 4000)
      .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran');
  },
  
  // Puntos 3 y 4
  'Dirigirse a la página de un profesor y Filtrar': function(browser) {
    browser
      .url('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/jose-tiberio-hernandez-penaloza')
	  .click('.materias input[name="ISIS2007"]')
	  .assert.containsText('.carreraCalificacion', 'Diseño Prod. E Innovación Ti')
      .end();
  }
};