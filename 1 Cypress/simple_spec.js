describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()

        //Lineas nuevas  
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail1@example.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })

    // 1.1 Registro
    it('Register user', function() {
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Omar")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Camacho")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oz.camacho10@uniandes.edu.co")
        cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select("Arquitectura")
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("123456")
        cy.get('.cajaSignUp').find('input[name="acepta"]').check()
        cy.get('.cajaSignUp').contains('Registrarse').click()
	cy.contains('Registro exitoso!')
	
    })

    // 1.2 Login Correcto
    it('Successful login', function() {
	cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("ocz.camacho10@uniandes.edu.co")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456")
        cy.get('.cajaLogIn').contains('Ingresar').click()
	cy.wait(2000)
	cy.contains('cuenta')
    })

    // 1.3 creación de una cuenta con un login que ya existe
    it('Creating an account with a login that already exists', function() {
	cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Omar")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Camacho")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("ocz.camacho10@uniandes.edu.co")
        cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select("Arquitectura")
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("123456")
        cy.get('.cajaSignUp').find('input[name="acepta"]').check()
        cy.get('.cajaSignUp').contains('Registrarse').click()
        cy.contains('Ya existe un usuario registrado con el correo')
    })

    // 2. Pruebe la funcionalidad de búsqueda de profesores
    // 3. Pruebe como dirigirse a la página de un profesor
    it("Teacher search, Go to a teacher's page", function() {
	cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select-input').find('input').type("jose ti", {force:true})
        cy.get('.Select-menu-outer').contains('Jose Tiberio Hernandez Penaloza').click()
        cy.get('.descripcionProfesor').contains('Jose Tiberio Hernandez Penaloza')
    })

    // 4. Pruebe los filtros por materia en la página de un profesor
    it("Subject filters on a teacher's page", function() {
        cy.get('.materias').find('input[name="ISIS2007"]').check()
    })
})
