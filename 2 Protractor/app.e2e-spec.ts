import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });

  // 1.
  it('should search a heroe', () => {
    page.navigateTo();
    expect(page.searchHeroe('Narco')).toEqual('Narco');
  });

  // 4.
  it('should navigate to heroe', () => {
    page.navigateTo();
    expect(page.navigateToHeroe()).toEqual('Save');
  });

  // 6.
  it('should navigate to heroe from search', () => {
    page.navigateTo();
    expect(page.navigateToHeroeFromSearch('Narco')).toEqual('Narco');
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

  // 2.
  it('should remove a heroe', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.removeHeroe('Dynama');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n - 1));
  });

  // 5.
  it('should navigate to heroe from heroes list', () => {
    page.navigateToHeroes();
    expect(page.navigateToHeroeFromHeroes('Celeritas')).toEqual('Save');
  });

  // 3.
  it('should edit heroe', () => {
    expect(page.editHeroe('Zero', 'HeroeModified')).toEqual('HeroeModified');
  });
});