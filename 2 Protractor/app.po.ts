import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  // 1.
  searchHeroe(nameHeroe: string) {
    element(by.tagName('input')).sendKeys(nameHeroe);
    return element(by.css('.search-result')).getText();
  }

  // 2.
  removeHeroe(nameHero: string) {
    var elemento = this.getElementHeroes(nameHero);
    elemento.element(by.tagName('button')).click();
  }

  // 3.
  editHeroe(heroToModified:string, heroe: string) {
    var elemento = this.getElementHeroes(heroToModified);
    elemento.click();
    element(by.buttonText('View Details')).click();

    element(by.tagName('input')).clear();
    element(by.tagName('input')).sendKeys(heroe);
    element(by.buttonText('Save')).click();
    this.navigateToHeroes();
   
    var elemento1 = this.getElementHeroes(heroe);

    return elemento1.all(by.tagName('span')).last().getText();
  }

  // 4.
  navigateToHeroe() {
    element.all(by.css('.col-1-4')).first().click();
    return element(by.buttonText('Save')).getText();
  }

  // 5.
  navigateToHeroeFromHeroes(nameHero: string) {
    var elemento = this.getElementHeroes(nameHero);
    elemento.click();

    element(by.buttonText('View Details')).click();
    return element(by.buttonText('Save')).getText();
  }

  // 6.
  navigateToHeroeFromSearch(nameHeroe: string) {
    element(by.tagName('input')).sendKeys(nameHeroe);
    element(by.css('.search-result')).click();
    return  element(by.tagName('input')).getAttribute('value');

  }
  
  // utilities
  getElementHeroes(heroe: string){
    var elemento =
    element(by.css('.heroes')).all(by.tagName('li')).filter(
      function(elem, index) {
        return elem.getText().then(function (text){
          return text.indexOf(heroe) !== -1;
        });
    }).first();

    return elemento;
  }
}